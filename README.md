# Keywords extraction challenge

Write a Python 3 package which generates the most important key-phrase (or key-words)

from a document based on a corpus.

In text folder you will find a zip archive with:

- one script file (script.txt)

- 3 transcript files (transcript1...3.txt)

Instructions:

1. Compute the most important key-words (a key-word can be between 1-3 words)

2. Choose the top n words from the previously generated list. Compare these key-
words with all the words occurring in all of the transcripts.

3. Generate a score (rank) for these top n words based on analysed transcripts

# Method

## 1 Extracting keywords 
1. Reading the raw text
  * Reading text line-by-line, and processing continuously
  * Separating each keyword candidate by stop words or punctuation marks. 
  * If any word contains punctuation mark like: (2014) then it would be separated to 3 elements: ['(', '2014', ')']
  * Multi word keywords are added to the with including sub-keywords, because if you have a keyword like: "red car" then "red", and "car" are also keywords
  * If any keyword ends with "s", then the last character is removed, because we assume that is in pruplar (note: it is a quite unelegant solution but in most cases it works, however if we have such keyword: "countries" then it would give us "countrie")
2. Merging raw keywords
  * After reading the whole text and placing keywords to array, many keywords could occure multiple times
  * It merges these keywords, but in the same time it create a list which includes scores regarding to the occurrance of keywords (so, if keyword "car" occures 10 times in the text, then the score would be 10)
  * In order to get the top n keywords, we have to sort our list (it uses reverse sorting)
  * After all this, we have the top n keyword of our text
  
## 2 Comparing texts
1. Getting common keywords of texts
  * In case, we want to get the common keywords of two texts, first we extract their keywords separetly (as it was described in section above), to get the original keywords and scores.
  * In order to get the common keywords of the two text files, it compares them and choose the matching ones. Not required to check sub-keywords because they are already included in the original keyword lists
  * In the same time, it calculates the scores of the common keywords. For score, it chooses the lower score from the two original score lists. It is reasonable, just consider, if you have two keywords, and score like: car:1210, and car:2 then together it has score: 2
   
  ![alt text](docs/text1.jpg)
2. Comparing connection of texts
  * In case we would like to compare 2 texts we should use their keywords
  * Usually length of these texts are different, which means probaly the number of keywords too.
  * For comparing we should calculate each keyword's realitve occurrance, which is given by normalizing the score lists. (the whole list is devided by the sum of list)
  * It is necessary, just image, you have a text with only one keyword, and score: banana:142 and an other text with: banana:10. Evidentaly they both about banana, so  they are 100% matching.
  * Then we execute same process as in section 2.1 but with these relative occurance. 

# Running of code
The code has not standard package dependency, but running test.py requires package "pytest".
```python
python3 main.py
top 5 keywords of script.txt
[['food'], ['animal'], ['include'], ['culture'], ['price']] [211, 29, 23, 22, 20]
Top 5 keywords of transcript_1.txt
[['food'], ['fast'], ['fast', 'food'], ['restaurant'], ['fish']] [119, 80, 69, 36, 14]
Top 5 common keywords of script.txt and transcript_1.txt
[['food'], ['world'], ['meat'], ['time'], ['countrie']] [119, 12, 10, 9, 8]
text1: 60.36%, text2: 37.42%
Top 5 keywords of transcript_1.txt
[['restaurant'], ['food'], ['guide'], ['meal'], ['serve']] [94, 37, 17, 13, 12]
Top 5 common keywords of script.txt and transcript_1.txt
[['food'], ['people'], ['common'], ['restaurant'], ['type']] [37, 8, 5, 5, 4]
text1: 29.16%, text2: 21.39%
Top 5 keywords of transcript_1.txt
[['food'], ['cooking'], ['meat'], ['vitamin'], ['fat']] [66, 58, 19, 17, 15]
Top 5 common keywords of script.txt and transcript_1.txt
[['food'], ['meat'], ['cooking'], ['vegetable'], ['raw']] [66, 18, 17, 13, 12]
text1: 42.21%, text2: 37.71%
```

