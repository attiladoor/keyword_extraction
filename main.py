from keywords.text_comparator import TextComparator
import os

FILES = '/mnt/storage/project/keywords/text/'

STOP_WORDS = os.path.join(FILES, 'stop_words.txt')
SCRIPT_FILE = os.path.join(FILES, 'script.txt')
TRANSCRIPT1_FILE = 'transcript_1.txt'
TRANSCRIPT2_FILE = 'transcript_2.txt'
TRANSCRIPT3_FILE = 'transcript_3.txt'
n = 5

print("top 5 keywords of script.txt")
comparator = TextComparator(SCRIPT_FILE, stop_words_path=STOP_WORDS)
key_w, score = comparator.get_text1_keywords(n)
print(key_w, score)

for file in [TRANSCRIPT1_FILE, TRANSCRIPT2_FILE, TRANSCRIPT3_FILE]:
    comparator = TextComparator(SCRIPT_FILE, os.path.join(FILES, file), STOP_WORDS)
    print("Top 5 keywords of {0}".format(TRANSCRIPT1_FILE))
    key_w, score = comparator.get_text2_keywords(n)
    print(key_w, score)

    print("Top 5 common keywords of script.txt and {0}".format(TRANSCRIPT1_FILE))
    key_w, score = comparator.get_common_keywords(n)
    print(key_w, score)
    p1, p2 = comparator.percentage_of_similarity(n)
    print('text1: {0:.2f}%, text2: {1:.2f}%'.format(p1, p2))


