from keywords.keywords import KeyWords


class TextComparator:

    def __init__(self, text1_path, text2_path=None, stop_words_path=None):

        self._keywords1 = KeyWords(text1_path, stop_words_path)
        self._keywords2 = KeyWords(text2_path, stop_words_path)
        self._text1_keywords, self.text1_scores = self._keywords1.calculate_top_n_keywords()
        if text2_path:
            self._text2_keywords, self.text2_scores = self._keywords2.calculate_top_n_keywords()
        self._common_keywords = KeyWords()

    """
      _compare_keywords: compare the keywords of texts, it marks as a common keyword only equals. There is no
        need to check sub keywords, because at generating of keywords in KeyWords class, every sub keyword of a
        multi word keyword included in the merged_keyword_list, so if you have such keyword: ['yellow', 'car']
        then either yellow and car are in the list. So in this function is no need for double-check

        @n: the function compares only the first n element of the lists
        @normalized: whether you want your data to be normalized, which is used at comparing the rate of
            two keywords set
    """

    def _compare_keywords(self, n=None, normalized=False):

        try:
            self._common_keywords = KeyWords()
            if normalized:
                text1_s = [x / sum(self.text1_scores) for x in self.text1_scores]
                text2_s = [x / sum(self.text2_scores) for x in self.text2_scores]
            else:
                text1_s = self.text1_scores
                text2_s = self.text2_scores

            for kw1 in self._text1_keywords[:n]:
                for kw2 in self._text2_keywords[:n]:
                    if kw1 == kw2:
                        c_score = min([text1_s[self._text1_keywords.index(kw1)],
                                           text2_s[self._text2_keywords.index(kw2)]])
                        self._common_keywords.add_element_to_merged_list(kw1, c_score)
            self._common_keywords.sort_keywords_list()
        except AttributeError:
            print("text2 file is missing")
            exit()

    """
        @n: comparing only the first top n keywords
        return the rate of keywords for each text, like: common_keywords/text1_keywords, common_keywords/text2_keywords
    """
    def percentage_of_similarity(self, n=None):

        self._compare_keywords(n, normalized=True)
        c_kw, c_score = self._common_keywords.get_merged_list()
        text1_percentage = sum(c_score)/sum([x / sum(self.text1_scores) for x in self.text1_scores][:n])*100
        text2_percentage = sum(c_score)/sum([x / sum(self.text2_scores) for x in self.text2_scores][:n])*100
        return text1_percentage, text2_percentage

    """
        getter functions
    """

    def get_common_keywords(self, n=None):
        self._compare_keywords()
        return self._common_keywords.get_merged_list(n)

    def get_text1_keywords(self, n=None):
        return self._keywords1.get_merged_list(n)

    def get_text2_keywords(self, n=None):
        return self._keywords2.get_merged_list(n)
