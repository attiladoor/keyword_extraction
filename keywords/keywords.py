from keywords.stop_words import Separator

class KeyWords:

    def __init__(self, text_file_path=None, stop_word_path=None):
        if stop_word_path:
            self._separator = Separator(stop_word_path)
        self._text_file_path = text_file_path
        self._buffer = []
        self._merged_keywords_list = []
        self._merged_keywords_scoreboard = []
        self._raw_keyword_list = []

    def calculate_top_n_keywords(self, n=None):
        self._read_text_to_array()
        self._merge_keywords_list()
        self.sort_keywords_list()

        return self._merged_keywords_list[:n], self._merged_keywords_scoreboard[:n]


    """
       merge_keywords_list: merges the possible keywords, and put it in one array
       @return: no return value, but it adds the merged keywords to the class, and a scoreboard which includes the
        occurrence of the keywords

        note1: it removes ever 's' character at the end of any sub keyword
        note2: if there is a keyword "black car" and "car" separately, then keyword "car" get one extra
            score, because it occurres in an other keyword too
    """

    def _merge_keywords_list(self):

        self._merged_keywords_list = []
        self._merged_keywords_scoreboard = []
        #pre-merge
        for kw in self._raw_keyword_list:
            for sub_kw in kw:
                if sub_kw[-1] == 's':
                    kw[kw.index(sub_kw)] = sub_kw[:-1]

            if kw in self._merged_keywords_list:
                self._merged_keywords_scoreboard[self._merged_keywords_list.index(kw)] += 1
            if kw not in self._merged_keywords_list:
                self._merged_keywords_list.append(kw)
                self._merged_keywords_scoreboard.append(1)

    """
        sort_keywords_list: it performs a sorting on the merged keywords list, it puts the most common keyword to
            the beginning of the list, and the less to the end. In parallel it performs the same on the scoreboard list
    """

    def sort_keywords_list(self):

        for i in range(len(self._merged_keywords_scoreboard)):

            max_index = self._merged_keywords_scoreboard[i:].index(max(self._merged_keywords_scoreboard[i:]))+i
            self._merged_keywords_list[i], self._merged_keywords_list[max_index] = \
                self._merged_keywords_list[max_index], self._merged_keywords_list[i]

            self._merged_keywords_scoreboard[i], self._merged_keywords_scoreboard[max_index] = \
                self._merged_keywords_scoreboard[max_index], self._merged_keywords_scoreboard[i]

    """
        read_text_to_array: reading text from file, and separates sub sentences according to the marks, and stop words
        @return: no return value, but at start it deletes all the keywords from keywords list, and later updates the list by
        the new elements. The data is possibly redundant in the list. It adds every sub-keyword of every keyword so if
        have: ['red','car'] then your raw keyword list would contain 'red' and 'car' too
    """

    def _read_text_to_array(self):
        if not self._text_file_path:
            return False

        self._raw_keyword_list = []
        with open(self._text_file_path) as f:
            for line in f:
                try:
                    self._add_element_to_buffer(line)
                    i = 0
                    while True:
                        self._buffer[i] = self._buffer[i].lower()

                        if self._separator.is_punctuation_mark(self._buffer[i]) or self._separator.is_stop_word(self._buffer[i]):
                            if self._buffer[:i]:
                                self._raw_keyword_list.append(self._buffer[:i])
                                # add every sub keyword of every multi word keyword
                                if len(self._buffer[:i]) > 1: #single words
                                    for sub_kw in self._buffer[:i]:
                                        self._raw_keyword_list.append([sub_kw])
                                if len(self._buffer[:i]) > 2: #2 long sub-keywords
                                    for j in range(len(self._buffer[:i])-2):
                                        self._raw_keyword_list.append(self._buffer[:i][j:j+2])
                            self._buffer = self._buffer[i + 1:]
                            i = 0
                        else:
                            i += 1
                except IndexError:
                    pass

    """
        add_element_to_buffer: separates the words from marks, and append them to class's buffer variable
        @in_string: one line of raw text data, where words and punctuation marks are not separated

    """
    def _add_element_to_buffer(self, in_string):
        if not self._separator:
            return False

        string_array = in_string.replace("\n", "").split(" ")
        str_index = 0
        while str_index < len(string_array):
            # if the punctuation mark is the first character->instert it before the word
            if self._separator.is_punctuation_mark(string_array[str_index][0]) and len(string_array[str_index]) > 1:
                string_array.insert(str_index+1, string_array[str_index][1:])
                string_array[str_index] = string_array[str_index][0]

            if self._separator.is_punctuation_mark(string_array[str_index][-1]) and len(string_array[str_index]) > 1:
                string_array.insert(str_index + 1, string_array[str_index][-1])
                string_array[str_index] = string_array[str_index][:-1]
                str_index -= 1

            str_index += 1

        for x in string_array:
            if x:
                self._buffer.append(x)
    """
        add_element_to_merged_list: append an element to the merged keywords list, and the related scoreboard list
            but only in case it is not in list yet, otherwise, it increases the score
    """
    def add_element_to_merged_list(self, keyword, score):
        if keyword in self._merged_keywords_list:
            self._merged_keywords_scoreboard[self._merged_keywords_list.index(keyword)] += score
        else:
            self._merged_keywords_list.append(keyword)
            self._merged_keywords_scoreboard.append(score)

    """
        get_n_of_merged_list: get the top n element of merged keywords list, and the related scores
    """
    def get_merged_list(self, n=None):
        return self._merged_keywords_list[:n], self._merged_keywords_scoreboard[:n]


