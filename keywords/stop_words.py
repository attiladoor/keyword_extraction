class Separator:

    def __init__(self, stop_words_list_path):
        self.path = stop_words_list_path
        self.list_of_marks = ['.', ',', ';', ':', "-", "–", "?", "!", "/", "\\", "\"", "'", "`",
                         "[", "]", "(", ")", "{", "}", "..."]

    """
    @target_word: our input word which we would like evaluate, if it is a stop word or not
    @return: boolean value which defines whether it is stop word or not
    """

    def is_stop_word(self, target_word):
        with open(self.path, 'r') as f:
            for line in f:
                if line[-1] == "\n":
                    line = line[0:-1]
                if target_word == line:
                    return True

        return False

    """
    @target_word: our input character which we would like evaluate, if it is a punctuation mark or not
    @return: boolean value which defines whether it is punctuation mark
    """

    def is_punctuation_mark(self, target_word):
        if target_word in self.list_of_marks:
            return True
        else:
            return False
