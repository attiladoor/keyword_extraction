from keywords.stop_words import Separator
from keywords.keywords import KeyWords
import unittest

STOP_WORDS = '/mnt/storage/project/keywords/text/stop_words.txt'
TEXT_FILE = '/mnt/storage/project/keywords/text/script.txt'

stop_w = Separator(STOP_WORDS)
key_w = KeyWords(TEXT_FILE, STOP_WORDS)


class TestIsStopWord(unittest.TestCase):
    # Separator.is_stop_word

    def test_isw1(self):
        self.assertTrue(stop_w.is_stop_word('a'))
    
    def test_isw2(self):
        self.assertTrue(stop_w.is_stop_word('must'))

    def test_isw3(self):
        self.assertFalse(stop_w.is_stop_word('asd'))
    

class TestIsPunctuationMark(unittest.TestCase):
    # Separator.is_punctuation_mark

    def test_ipm1(self):
        self.assertTrue(stop_w.is_punctuation_mark('?'))

    def test_ipm2(self):
        self.assertTrue(stop_w.is_punctuation_mark('...'))

    def test_ipm3(self):
        self.assertFalse(stop_w.is_punctuation_mark('|'))

    def test_ipm4(self):
        self.assertFalse(stop_w.is_punctuation_mark('dog?'))


class TestAddElementToBuffer(unittest.TestCase):
    # KeyWords.test_add_element_to_buffer

    def test_add_element_to_buffer1(self):
        key_w._buffer = []
        key_w._add_element_to_buffer("adam!")
        self.assertEqual(len(key_w._buffer), 2)

    def test_add_element_to_buffer2(self):
        key_w._buffer = []
        key_w._add_element_to_buffer("(adam!")
        self.assertEqual(len(key_w._buffer), 3)

    def test_add_element_to_buffer3(self):
        key_w._buffer = []
        key_w._add_element_to_buffer("(adam!")
        self.assertEqual(key_w._buffer, ['(', 'adam', '!'])

    def test_add_element_to_buffer4(self):
        key_w._buffer = []
        key_w._add_element_to_buffer("(ada;m!")
        self.assertEqual(key_w._buffer, ['(', 'ada;m', '!'])


class TestMergeKeywordsList(unittest.TestCase):
    #KeyWords.testmerge_keywords_list

    def test_merge_keywords_list1(self):
        key_w._raw_keyword_list = ['apple', 'banana', 'car', 'banana']
        key_w._merge_keywords_list()
        self.assertEqual(key_w._merged_keywords_list, ['apple', 'banana', 'car'])

    def test_merge_keywords_list2(self):
        key_w._raw_keyword_list = ['apple', 'banana', 'car', 'banana']
        key_w._merge_keywords_list()
        self.assertEqual(key_w._merged_keywords_scoreboard, [1, 2, 1])

    def test_merge_keywords_list3(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow','banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        self.assertEqual(key_w._merged_keywords_list, [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                               ['green', 'banana']])

    def test_merge_keywords_list4(self):
        key_w._raw_keyword_list = [['red', 'apples'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        self.assertEqual(key_w._merged_keywords_scoreboard, [1, 2, 1, 1])


class TestSortKeywordsList(unittest.TestCase):
    #KeyWords.sort_keywords_list

    def test_sort_keywords_list1(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'bananas'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        key_w.sort_keywords_list()
        self.assertEqual(key_w._merged_keywords_scoreboard, [2, 1, 1, 1])

    def test_sort_keywords_list2(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        key_w.sort_keywords_list()
        self.assertEqual(key_w._merged_keywords_list[0], ['yellow', 'banana'])

    def test_sort_keywords_list3(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana'], ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        key_w.sort_keywords_list()
        self.assertEqual(key_w._merged_keywords_list[1], ['green', 'banana'])

    def test_sort_keywords_list4(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana'], ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        key_w.sort_keywords_list()
        self.assertEqual(key_w._merged_keywords_scoreboard, [3, 2, 1, 1])


class TestAddElementToMergedList(unittest.TestCase):
    #KeyWords.add_element_to_merged_list

    def test_add_element_to_merged_list1(self):
        key_w._merged_keywords_list = [['red', 'apple'], ['pink', 'bananas'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']]
        key_w._merged_keywords_scoreboard = [1, 1, 1, 1, 1]

        key_w.add_element_to_merged_list(['red', 'apple'], 1)
        self.assertEqual(key_w._merged_keywords_list, [['red', 'apple'], ['pink', 'bananas'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']])

    def test_add_element_to_merged_list2(self):
        key_w._merged_keywords_list = [['red', 'apple'], ['pink', 'bananas'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana']]

        key_w.add_element_to_merged_list(['red', 'car'], 1)
        self.assertEqual(key_w._merged_keywords_list, [['red', 'apple'], ['pink', 'bananas'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana'], ['red', 'car']])

    def test_add_element_to_merged_list3(self):
        key_w._raw_keyword_list = [['red', 'apple'], ['yellow', 'banana'], ['fancy', 'black', 'car'],
                                   ['yellow', 'banana'], ['green', 'banana'], ['yellow', 'banana'], ['green', 'banana']]
        key_w._merge_keywords_list()
        key_w.sort_keywords_list()
        key_w.add_element_to_merged_list(['yellow', 'banana'], 1)
        self.assertEqual(key_w._merged_keywords_scoreboard, [4, 2, 1, 1])


if __name__ == '__main__':
    unittest.main()

